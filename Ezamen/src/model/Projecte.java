/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author jorge
 */
public class Projecte implements Serializable {
    private int codproj;
    private String nom;
    private int lloc;
    private Departament dep;

    public Projecte(String nom, int lloc, Departament dep) {
        this.nom = nom;
        this.lloc = lloc;
        this.dep = dep;
    }

    public int getCodproj() {
        return codproj;
    }

    public void setCodproj(int codproj) {
        this.codproj = codproj;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getLloc() {
        return lloc;
    }

    public void setLloc(int lloc) {
        this.lloc = lloc;
    }

    public Departament getDep() {
        return dep;
    }

    public void setDep(Departament dep) {
        this.dep = dep;
    }
    
    
    
}
