/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author jorge
 */
public class Departament implements Serializable {
    private String dep;
    private String nom;
    private Date data;
    private int hores;
    private Aula codaula;
    private Set<Projecte> projecte = new HashSet<>();
    
    public Departament(String dep, String nom, Date data, int hores) {
        this.dep = dep;
        this.nom = nom;
        this.data = data;
        this.hores = hores;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getHores() {
        return hores;
    }

    public void setHores(int hores) {
        this.hores = hores;
    }

    public Set<Projecte> getProjecte() {
        return projecte;
    }

    public void setProjecte(Set<Projecte> projecte) {
        this.projecte = projecte;
    }

    public Aula getAula() {
        return codaula;
    }

    public void setAula(Aula codaula) {
        this.codaula = codaula;
    }

    public Aula getCodaula() {
        return codaula;
    }

    public void setCodaula(Aula codaula) {
        this.codaula = codaula;
    }
    
    
}
