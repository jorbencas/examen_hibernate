/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author jorge
 */
public class Aula implements Serializable {
    private int codaula;
    private String nom;

    public Aula(int codaula,String nom) {
        this.codaula = codaula;
        this.nom = nom;
    } 

    public int getCodaula() {
        return codaula;
    }

    public void setCodaula(int codaula) {
        this.codaula = codaula;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
}
